package main

import (
	"fmt"
	"math"
	"math/rand"
)

const(
	LAMBDA = 0.5
)

func targetFunc(x float64) float64 {
	return x*x
	//return -math.Cos(x)*math.Cos(3*x)
}

func Sum(s []float64) float64 {
	ret := 0.0

	for _, v := range s {
		ret += v
	}
	return ret
}

func cross(entities, chances []float64, precision int32) float64 {
	n1 := 0
	n2 := 0

	for n1 == n2 {

		randValue := rand.Float64()
		total := 0.0

		for j := range chances {
			total += chances[j]
			if randValue < total {
				n1 = j
				break
			}
		}

		randValue = rand.Float64()
		total = 0.0
		for j := range chances {
			total += chances[j]

			if randValue < total {
				n2 = j
				break
			}
		}
	}

	/*cutLine := int(rand.Int31n(precision))
	powValue := 1.0
	if cutLine > 0 {
		powValue = math.Pow10(cutLine)
	}*/
	// cutLine 2      1.345 ----> multiply ---> 134.5 ---> floor(x) --> 134 ----> divide ---> 1.34
	e1 := (1 - LAMBDA) * entities[n1] + LAMBDA * entities[n2]
	e2 := (1 + LAMBDA) * entities[n1] - LAMBDA * entities[n2]
	e3 := -LAMBDA * entities[n1] + (1 + LAMBDA) * entities[n2]
	newEntity := math.Min(math.Min(targetFunc(e1), targetFunc(e2)), targetFunc(e3))

	if newEntity == targetFunc(e1) {
		return e1
	} else if newEntity == targetFunc(e2) {
		return e2
	} else {
		return e3
	}

	/*if newEntity < 0 {
		newEntity -= entities[n2] - math.Floor(entities[n2]*(powValue))/(powValue)
	} else {
		newEntity += entities[n2] - math.Floor(entities[n2]*(powValue))/(powValue)
	}*/

//	return newEntity
}

func sel(entities, chances []float64) float64 {
	total := 0.0
	randValue := rand.Float64()
	ret := 0.0
	for j := range chances {
		total += chances[j]

		if randValue < total {
			ret = entities[j]
			break
		}
	}

	return ret
}

func mutate(value float64) float64 {
	return rand.NormFloat64() * value/10.0 + value
}

func findMin(values []float64) float64 {
	value := values[0]

	for i := range values {
		value = math.Min(value, values[i])
	}
	return value
}

func findMax(values []float64) float64 {
	value := values[0]

	for i := range values {
		value = math.Max(value, values[i])
	}
	return value
}

func main() {

	lowerBound := -1.0
	upperBound := 3.0

	var precision int32 = 16
	entities := make([]float64, 100)
	for i := range entities {
		entities[i] = (rand.Float64() * (upperBound - lowerBound)) - lowerBound
	}

	pCross := 0.6
	pMutate := 0.05

	for n := 0; n < 100; n++ {
		var values []float64
		for _, v := range entities {
			values = append(values, 1.0 / targetFunc(v))
		}
		sum := Sum(values)
		var chances []float64
		for i := range entities {
			chances = append(chances, values[i] / sum)
		}

		newEntities := entities
		// new entities
		for i := 0; i < int(math.Floor(float64(len(entities)) * pCross)); i++ {
			newEntities[i] = cross(entities, chances, precision)
			for newEntities[i] < lowerBound || newEntities[i] > upperBound {
				newEntities[i] = cross(entities, chances, precision)
			}
		}

		// select the rest
		for i := int(math.Ceil(float64(len(entities)) * pCross)); i < len(entities); i++ {
			newEntities[i] = sel(entities, chances)
		}

		fmt.Println("Epoch", n, "min value:", findMin(newEntities), "max value", findMax(newEntities), "sum", Sum(newEntities))

		for i := range entities {
			if rand.Float64() < pMutate {
				newEntities[i] = mutate(newEntities[i])
				for newEntities[i] < lowerBound || newEntities[i] > upperBound {
					newEntities[i] = mutate(newEntities[i])
				}
			}
		}

		entities = newEntities

		fmt.Println("Epoch", n, "min value:", findMin(entities), "max value", findMax(entities), "sum", Sum(entities))
		//fmt.Println("Epoch", n, "value:", entities)
	}
}
