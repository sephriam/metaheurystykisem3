package main

import (
	"bytes"
	"fmt"
	"math/rand"
	"sort"
)

func initializeEntities(nodes[]Node) [][]byte {
	entities := make([][]byte, 100)
	edgesSet := make(map[byte]byte)

	for i := range entities {

		for _, v := range nodes {
			edgesSet[v.Number] = v.Number
		}
		for len(edgesSet) > 0 {
			n := rand.Int() % len(edgesSet)
			val := nthElementInMap(edgesSet, n)
			entities[i] = append(entities[i],val)
			delete(edgesSet, val)
		}
	}
	return entities
}

func calcDistance(entity []byte, edges []Edge, penalty int) int {

	cost := 0
	for i := 0; i < len(entity) - 1; i++ {
		cost += getDistance(edges, entity[i], entity[i+1], penalty)
	}
	return cost
}

func createNewPath(p1, p2 Path) Path {

	var newPath Path
	randVal := rand.Int() % len(p1.Nodes)
	// get n Nodes from p1
	newPath.Nodes = p1.Nodes[:randVal]

	// take rest from p2
	for i := range p2.Nodes {
		if n := bytes.IndexByte(newPath.Nodes, p2.Nodes[i]); n == -1 {
			newPath.Nodes = append(newPath.Nodes, p2.Nodes[i])
		}
	}

	return newPath
}

func cross(paths []Path, pCross float64) []Path {

	chances := make([]float64, len(paths))
	chancesSum := 0.0
	sum := float64(Sum(paths))
	// calculate the odds
	for i := range chances {
		chances[i] = 1.0 / (float64(paths[i].Distance) / sum)
		chancesSum += chances[i]
	}

	newPaths := make([]Path, len(paths))

	// rewrite the best ones
	for i := 0; i < int(float64(len(paths)) * (1 - pCross)); i++ {
		newPaths[i] = paths[i]
	}

	// cross the rest
	for i := int(float64(len(paths)) * (1 - pCross)); i < len(paths); i++ {

		cChance := 0.0
		randVal := rand.Float64() * chancesSum
		n1 := 0
		n2 := 0
		j := 0
		for cChance < chancesSum {
			cChance += chances[j]
			if cChance > randVal {
				n1 = j
				break
			}
			j++
		}

		cChance = 0.0
		randVal = rand.Float64() * chancesSum
		j = 0
		for cChance < chancesSum {
			cChance += chances[j]
			if cChance > randVal {
				n2 = j
				break
			}
			j++
		}

		for n1 == n2 {
			n2 = (n1 + 1) % len(paths)
		}

		newPaths[i] = createNewPath(paths[n1], paths[n2])
	}

	return newPaths
}

func mutate(paths *[]Path, pMutate float64) {

	for i := range *paths {

		if pMutate > rand.Float64() {
			randVal1 := rand.Int() % len((*paths)[i].Nodes)
			randVal2 := rand.Int() % len((*paths)[i].Nodes)

			tmp := (*paths)[i].Nodes[randVal1]
			(*paths)[i].Nodes[randVal1] = (*paths)[i].Nodes[randVal2]
			(*paths)[i].Nodes[randVal2] = tmp
		}
	}
}

func main() {

	nodes, edges := readInput("pl_woj_sel.in")
	pCross := 0.6
	pMutate := 0.01

	entities := initializeEntities(nodes)
	paths := make([]Path, len(entities))
	for i := range paths {
		paths[i].Nodes = entities[i]
	}

	for n := 0; n < 1000; n++ {

		for i := range paths {
			paths[i].Distance = calcDistance(paths[i].Nodes, edges, 2000)
		}
		sort.Slice(paths, func(i,j int) bool {
			return PathCmp(&paths[i], &paths[j])
		})

		paths = cross(paths, pCross)
		mutate(&paths, pMutate)

	}

	for i := range paths {
		paths[i].Distance = calcDistance(paths[i].Nodes, edges, 2000)
	}
	sort.Slice(paths, func(i,j int) bool {
		return PathCmp(&paths[i], &paths[j])
	})

	fmt.Println(paths)
}
