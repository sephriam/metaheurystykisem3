package main

import (
	"fmt"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func readInput(filepath string) (nodes [] Node, edges [] Edge) {

	f, err := os.Open(filepath)
	check(err)
	var records int
	_, err = fmt.Fscanf(f, "%d\n", &records)
	fmt.Println("City Records:", records)
	check(err)

	for i := 0; i < records; i++ {
		var inNode Node
		_, err = fmt.Fscanf(f, "%d %c\n", &inNode.Number, &inNode.Name)
		check(err)
		nodes = append(nodes, inNode)
		//fmt.Println(inNode)
	}

	_, err = fmt.Fscanf(f, "%d\n", &records)
	fmt.Println("Edge Records:", records)
	for i := 0; i < records; i++ {
		var inEdge Edge
		_, err = fmt.Fscanf(f, "%d %d %d\n", &inEdge.begin, &inEdge.end, &inEdge.distance)
		check(err)
		edges = append(edges, inEdge)
		//fmt.Println(inEdge)
	}
	return
}

func nthElementInMap(arg map[byte]byte, n int) byte {

	if n >= len(arg) {
		n = 0
	}

	for v := range arg {
		if n == 0 {
			return v
		}

		n--
	}
	panic("Index out of range in nthElementMap")

	return 0
}

func getDistance(edges []Edge, begin, end byte, penalty int) int {

	for i := range edges {
		if (edges[i].begin == begin && edges[i].end == end) || (edges[i].begin == end && edges[i].end == begin) {
			return edges[i].distance
		}
	}
	return penalty
}

func Sum(paths []Path) int {
	sum := 0
	for _, v := range paths {
		sum += v.Distance
	}
	return sum
}