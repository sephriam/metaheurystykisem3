package main

type Path struct  {
	Nodes []byte
	Distance int
}

func PathCmp(p1, p2 *Path) bool {
	return p1.Distance < p2.Distance
}