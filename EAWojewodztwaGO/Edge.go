package main

type Edge struct {
	begin byte
	end byte
	distance int
}
