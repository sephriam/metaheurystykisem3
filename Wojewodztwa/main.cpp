#include <iostream>
#include <vector>
#include <fstream>
#include <tuple>
#include <cmath>
#include <set>
#include "Edge.h"

std::tuple<std::vector<char>, std::vector<Edge>> readData(std::string fileName) {
    std::vector<char> vertices;
    std::vector<Edge> edges;
    std::ifstream inputFile(fileName);
    while(inputFile.is_open() && !inputFile.eof()) {
        unsigned int num = 0;
        inputFile >> num;
        for(unsigned int i = 0; i < num; ++i) {
            char c;
            int dummy;
            inputFile >> dummy >> c ;
            vertices.emplace_back(c);
        }

        inputFile >> num;
        for(unsigned int i = 0; i < num; ++i) {
            Edge e;
            inputFile >> e.begin >> e.end >> e.distance;
            edges.emplace_back(e);
        }
    }

    return std::make_tuple(vertices, edges);
}

std::vector<int> init(std::vector<char> vertices) {
    std::vector<int> ret;
    std::set<int> s;
    for(unsigned int i = 0; i < vertices.size(); ++i) {
        unsigned int r = rand() % vertices.size();
        if(s.find(r) == s.end()) {
            ret.push_back(r);
            s.insert(r);
        } else {
            --i;
        }
    }

    return ret;
}

std::vector<int> perm(std::vector<int> vec, unsigned int K) {

    for(unsigned int i = 0; i < K; ++i) {
        std::swap(vec[rand() % vec.size()], vec[rand() % vec.size()]);
    }

    return vec;
}


unsigned int calcCost(const std::vector<int>& path, const std::vector<Edge>& edges, unsigned int addCost) {

    unsigned int cost = 0;
    for(unsigned int i = 0; i < path.size() - 1; ++i) {
        int pos = -1;
        for(unsigned int j = 0; j < edges.size(); ++j) {
            if((edges[j].begin == path[i] && edges[j].end == path[i+1]) ||
                (edges[j].end == path[i] && edges[j].begin == path[i+1])) {
                pos = j;
            }
        }

        if(pos != -1) {
            cost += edges[pos].distance;
        } else {
            cost += addCost;
        }
    }
    return cost;
}

auto simulatedAnnealing(const std::vector<Edge>& edges, const std::vector<char>& vertices, int T0,
                                    unsigned int maxSteps, unsigned int addCost, unsigned int K) {

    auto ret = init(vertices);
    unsigned int steps = 0;
    unsigned int cost = calcCost(ret, edges, addCost);
    while(steps < maxSteps) {
        auto path = perm(ret, K);
        unsigned int newCost = calcCost(path, edges, addCost);

        double deltaE = static_cast<double>(newCost) - static_cast<double>(cost);
        if(deltaE < 0.0 || static_cast<double>(rand()) / RAND_MAX < std::exp(-deltaE/T0)) {
            ret = std::move(path);
            cost = newCost;
            std::cout << cost << '\n';
        }

        ++steps;
    }

    return std::make_tuple(ret, cost);
}

int main() {

    srand(time(nullptr));
    auto [vertices, edges] = readData("/home/wojtowic/workspace/sem3/Metaheurystyki/Wojewodztwa/pl_woj_sel.in");

    for(const auto& it : vertices) {
        //std::cout << it << '\n';
    }
    //vertices.erase(vertices.end() - 1);

    for(const auto& it : edges) {
        //std::cout << it.begin << " " << it.end << " " << it.distance << '\n';
    }

    auto [path, cost] = simulatedAnnealing(edges, vertices, 1000, 1000, 2000, 2);
    for(const auto& it : path) {
        std::cout << vertices[it] << "->";
    }
    std::cout << '\n' << cost << '\n';

    return 0;
}
