//
// Created by wojtowic on 20.10.18.
//

#ifndef WOJEWODZTWA_EDGE_H
#define WOJEWODZTWA_EDGE_H


class Edge {
public:
    unsigned int begin;
    unsigned int end;
    unsigned int distance;

};


#endif //WOJEWODZTWA_EDGE_H
