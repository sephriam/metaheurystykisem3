#include <iostream>
#include <cmath>
#include <time.h>
#include <cstdlib>
#include "SimulatedAnnealing.h"

double func(double x) {
	return -cos(x)*cos(3 * x);
}

double func2(double x) {
    return 1 + ((x*x)/4000.0) - cos(x);
}

int main()
{
	srand(time(0));
	SimulatedAnnealing s(func, { -M_PI_2, M_PI_2 });
    //SimulatedAnnealing s(func2, { -20, 20 });
	unsigned int global = 0;
	//double meanVal = 0.0;
	for(int i = 0; i < 10; ++i) {
        double x = s.findMinDummy(((static_cast<double>(rand()) / RAND_MAX) * M_PI) - M_PI_2, 1000, 0.5);
        //double x = s.findMinDummy(((static_cast<double>(rand()) / RAND_MAX) * 40) - 20, 1000, 10);
        //std::cout << x << ", " << func(x) << std::endl;
        if(x < 1.0 && x > -1.0) {
            ++global;
        }
    }

    std::cout << "Global: " << global << std::endl;
    global = 0;
    for(int i = 0; i < 10; ++i) {
        double x = s.findMin(((static_cast<double>(rand()) / RAND_MAX) * M_PI) - M_PI_2, 1000, 0.5);
        //double x = s.findMin(((static_cast<double>(rand()) / RAND_MAX) * 40) - 20, 1000, 10);
        //std::cout << x << ", " << func(x) << std::endl;

        if(x < 1.0 && x > -1.0) {
            ++global;
        }
    }
    std::cout << "Global Prob: " << global << std::endl;

    global = 0;
    for(int i = 0; i < 10; ++i) {
        double x = s.findMinGauss(((static_cast<double>(rand()) / RAND_MAX) * M_PI) - M_PI_2, 1000, 0.75, 0.975, 5, 0.98, 1, 0.5);
        //double x = s.findMinGauss(((static_cast<double>(rand()) / RAND_MAX) * 40) - 20, 1000, 0.75, 0.975, 5, 0.98, 1, 5);
        //std::cout << x << ", " << func(x) << std::endl;

        if(x < 1.0 && x > -1.0) {
            ++global;
        }
    }
    std::cout << "Global Gauss: " << global << std::endl;

    return 0;
}

