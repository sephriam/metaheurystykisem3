#include "SimulatedAnnealing.h"

#include <cmath>
#include <iostream>
#include <random>

SimulatedAnnealing::SimulatedAnnealing(std::function<double(double)> objFunc, std::pair<double, double> constrains) :
	m_objFunc(objFunc),
	m_constrains(std::move(constrains))
{
}

double SimulatedAnnealing::findMin(double x0, unsigned int maxSteps, double delta)
{
	double y0 = m_objFunc(x0);
    double alpha = 0.95;
    double T0 = 0.75;
    unsigned int steps = 0;

	while (steps < maxSteps) {
        //std::cout << "x=" << x0 << " y=" << m_objFunc(x0) << std::endl;
		double x1 = x0 + ((static_cast<double>(rand()) / RAND_MAX) * (2 * delta)) - delta;
		if (x1 > m_constrains.second) {
			x1 = m_constrains.second - (x1 - m_constrains.second);
		} else if (x1 < m_constrains.first) {
			x1 = m_constrains.first + (x1 + m_constrains.first);
		}

		double y1 = m_objFunc(x1);
		double deltaE = y1 - y0;
		if (deltaE < 0) {
			x0 = x1;
		} else if((static_cast<double>(rand()) / RAND_MAX) < std::exp(-deltaE/T0)) {
            x0 = x1;
		}
		++steps;

		if (steps % 10 == 0) {
			delta *= 0.9;
		}

		if(steps % 7 == 0) {
		    T0 *= alpha;
		}
	}

	//std::cout << T0 << "," << delta << '\n';

	return x0;
}

double SimulatedAnnealing::findMinDummy(double x0, unsigned int maxSteps, double delta) {
	double y0 = m_objFunc(x0);
    unsigned int steps = 0;

	while (steps < maxSteps) {
        //std::cout << "x=" << x0 << " y=" << m_objFunc(x0) << std::endl;
		double x1 = x0 + ((static_cast<double>(rand()) / RAND_MAX) * (2 * delta)) - delta;
        if (x1 > m_constrains.second) {
            x1 = m_constrains.second - (x1 - m_constrains.second);
        } else if (x1 < m_constrains.first) {
            x1 = m_constrains.first + (x1 + m_constrains.first);
        }

		double y1 = m_objFunc(x1);
		double deltaE = y1 - y0;
		if (deltaE < 0) {
			x0 = x1;
		}
		++steps;

		if (steps % 10 == 0) {
			delta *= 0.9;
		}
	}

	return x0;
}

double SimulatedAnnealing::findMinGauss(double x0, unsigned int maxSteps, double T0, double eta, unsigned int feta,
                                        double alpha, unsigned int falpha, double delta) {
    double y0 = m_objFunc(x0);
    unsigned int steps = 0;

    std::random_device rd{};
    std::mt19937 gen{rd()};
    // values near the mean are the most likely
    // standard deviation affects the dispersion of generated values from the mean

    while (steps < maxSteps) {
        //std::cout << "x=" << x0 << " y=" << m_objFunc(x0) << std::endl;
        std::normal_distribution<> d{x0,delta};
        double x1 = d(gen);
        if (x1 > m_constrains.second) {
            x1 = m_constrains.second - (x1 - m_constrains.second);
        } else if (x1 < m_constrains.first) {
            x1 = m_constrains.first + (x1 + m_constrains.first);
        }

        double y1 = m_objFunc(x1);
        double deltaE = y1 - y0;
        if (deltaE < 0) {
            x0 = x1;
        } else if((static_cast<double>(rand()) / RAND_MAX) < std::exp(-deltaE/T0)) {
            x0 = x1;
        }
        ++steps;

        if (steps % feta == 0) {
            delta *= eta;
        }

        if(steps % falpha == 0) {
            T0 *= alpha;
        }
    }

    //std::cout << T0 << "," << delta << '\n';

    return x0;
}
