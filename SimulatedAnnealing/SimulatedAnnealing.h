#pragma once

#include <functional>

class SimulatedAnnealing {
public:
	SimulatedAnnealing(std::function<double(double)> objFunc, std::pair<double, double> constrains);
	double findMinDummy(double x0, unsigned int maxSteps, double delta = 0.5);
	double findMin(double x0, unsigned int maxSteps, double delta = 0.5);
    double findMinGauss(double x0, unsigned int maxSteps, double T0, double eta, unsigned int feta,
            double alpha, unsigned int falpha, double delta = 0.5);

private:
	std::function<double(double)> m_objFunc;
	std::pair<double, double> m_constrains;
};